/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usart.h"
#include "stdio.h"
#include "delay.h"
#include "oled.h"
#include "spi.h"
#include "w25qxx.h"
#include "ff.h"
#include "diskio.h"
#include "ffconf.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
FATFS fs;                       /* FatFs 文件系统对象 */
FIL file;                       /* 文件对象 */
FRESULT f_res;                  /* 文件操作结果 */
UINT fnum;                      /* 文件成功读写数量 */
BYTE ReadBuffer[1024] = {0};    /* 读缓冲区 */
BYTE WriteBuffer[] =            /* 写缓冲区 */
    "This is STM32 working with FatFs\r\n";
BYTE work[FF_MAX_SS];



char rData[4096]="";//读取的数据
UINT br;
void write_test(void){
	int res = 0;
	const char wData[]="欢迎来到我的世界，我的公主殿下！";//写入的数据
	UINT bw;
	res = f_open(&file,"0:first.txt",FA_OPEN_ALWAYS|FA_READ|FA_WRITE);
	printf("f_open函数的返回值是%d\n",res);
	if(res == FR_OK)//如果f_open函数打开成功，就会返回FR_OK,这个时候我们开始写入函数
	{
		//参数1：文件结构指针。参数2：写入的内容，参数3：写入内容有多大。参数4：已经写了多少个
		res = f_write(&file,wData,sizeof(wData),&bw);
		printf ("\r\n	bw= %d",bw);	//打印已经写了多少个数据,有多少个数据写入正常了
		if(res == FR_OK)//如果写入成功，再读取
		{
			//再读取之前，要先把光标指在首地址，这样才能把文本内容读取到，不然读取到的内容是你打开文本后的最后一个数据的后面的内容
			f_lseek(&file,0);//参数1：文件结构体指针；参数2：光标指到哪里
			res = f_read(&file,rData,f_size(&file),&br);//参数1：文件结构体指针。参数2：读取到的数据存放在哪里的缓冲区。参数3：读取内容大小，我们这里读取整个文件大小的内容，所以用了f_size函数;参数4：读到了多少内容
			if(res == FR_OK)
			{
				printf ("\r\n文件内容:%s br= %d",rData,br);	//打印已经读了多少个数据，打印已经读取了多少个正常的数据
		
			}
		}
		//读完了就关闭文件
		f_close(&file);
	}
}

void read_test(void){
	int res = 0;
	res = f_open(&file,"0:first.txt",FA_OPEN_ALWAYS|FA_READ);
	printf("f_open函数的返回值是=========%d\r\n",res);
	if(res == FR_OK){
		res = f_read(&file,rData,f_size(&file),&br);//参数1：文件结构体指针。参数2：读取到的数据存放在哪里的缓冲区。参数3：读取内容大小，我们这里读取整个文件大小的内容，所以用了f_size函数;参数4：读到了多少内容
			if(res == FR_OK)
			{
				printf ("\r\n文件内容:%s br= %d\r\n",rData,br);	//打印已经读了多少个数据，打印已经读取了多少个正常的数据
		
			}
	}
	
		f_close(&file);
	
}

int main(void)
{
  HAL_Init();
  SystemClock_Config();
  MX_GPIO_Init();
	Delay_Init(72);
	USART_Init(115200);
	SPI1_Init();
	OLED_Init();
	W25QXX_Init();
	
	printf("W25QXX_ReadID = 0x%x\r\n",W25QXX_ReadID());
	printf("W25QXX_ID = %d\r\n",W25QXX_ID);
	OLED_ShowString(0,0,"string");
	
	 printf("\r\n****** FatFs Example ******\r\n\r\n");
    
  f_res = f_mount(&fs, "0:", 1);
	printf("f_mount res = %d\r\n",f_res);
	if(f_res == FR_NO_FILESYSTEM){
		f_res = f_mkfs("0:",0,work,sizeof(work));
		printf("f_mkfs res = %d\r\n",f_res);
		f_res = f_mount(NULL,"0:",1);
		f_res = f_mount(&fs,"0:",1);	
	}
	
	//write_test();
	read_test();
  while (1)
  {
		
    delay_ms(1000);
		//read_test();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStructure.Pull= GPIO_PULLUP;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStructure.Pin = GPIO_PIN_4;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
