/*-----------------------------------------------------------------------*/
/* Low level disk I/O module SKELETON for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */
#include "w25qxx.h"
#include "stdio.h"
/* Definitions of physical drive number for each drive */
#define DEV_FLASH		0	/* Example: Map Ramdisk to physical drive 0 */
#define DEV_MMC		1	/* Example: Map MMC/SD card to physical drive 1 */
#define DEV_USB		2	/* Example: Map USB MSD to physical drive 2 */


/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat;
	int result;

	switch (pdrv) {
	case DEV_FLASH :
		result = W25QXX_ReadID();
	  if(result == W25Q64){
			stat = 0;
		}else{
			stat = STA_NOINIT;
		}
		return stat;

	case DEV_MMC :
		return stat;

	case DEV_USB :
		return stat;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat;
  int result;
	int i = 500;
	switch (pdrv) {
	case DEV_FLASH :
		W25QXX_Init();
	  while(--i);
	  W25QXX_WAKEUP();
	  result = disk_status(DEV_FLASH);
	  stat = result;
		return stat;

	case DEV_MMC :
		return stat;

	case DEV_USB :
		return stat;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	LBA_t sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
	DRESULT res;
	//int result;

	switch (pdrv) {
	case DEV_FLASH :
		sector+=512;
		W25QXX_Read(buff, sector <<12, count<<12);
	  res = RES_OK;
		return res;

	case DEV_MMC :
		return res;

	case DEV_USB :
		return res;
	}

	return RES_PARERR;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	LBA_t sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
	DRESULT res;
	uint32_t write_addr;
//	int result;

	switch (pdrv) {
	case DEV_FLASH :
		sector+=512;
	  write_addr = sector<<12;
	  W25QXX_Erase_Sector(write_addr);
		W25QXX_SectorWrite((uint8_t *)buff,write_addr,count<<12);
		res = RES_OK;
		return res;
	case DEV_MMC :
		return res;
	case DEV_USB :
		return res;
	}

	return RES_PARERR;
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res;
//	int result;

	switch (pdrv) {
	case DEV_FLASH :
		switch(cmd){
			case GET_SECTOR_SIZE:
				*(DWORD*)buff = 4096;
				res = RES_OK;
			break;
			
			case GET_BLOCK_SIZE:
				*(DWORD*)buff = 1;
				res = RES_OK;
			break;
			
			case GET_SECTOR_COUNT:
				*(DWORD*)buff = 2048;
				res = RES_OK;
			break;
			
			default:
				res = RES_PARERR;
			break;
		}
		return res;

	case DEV_MMC :

		// Process of the command for the MMC/SD card

		return res;

	case DEV_USB :

		// Process of the command the USB drive

		return res;
	}

	return RES_PARERR;
}

