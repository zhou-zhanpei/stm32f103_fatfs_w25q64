#ifndef __SPI__H__
#define __SPI__H__

#ifdef __cplusplus
extern "C" {
#endif



#include "stm32f1xx.h"
#include "stm32f103xe.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_conf.h"



#define SPI1_SCK           GPIO_PIN_5
#define SPI1_MISO          GPIO_PIN_6
#define SPI1_MOSI          GPIO_PIN_7


void SPI1_Init(void);			 //初始化SPI1口 
void SPI1_Setclock(uint8_t SPI_Prescaler);
uint8_t SPI1_ReadWriteByte(uint8_t writeData);  //SPI1总线读写一个字节


#ifdef __cplusplus
}
#endif

#endif



