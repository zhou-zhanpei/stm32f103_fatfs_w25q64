#ifndef __DELAY_H__
#define __DELAY_H__

#ifdef __cplusplus
extern "C" {
#endif
#include "stm32f1xx.h"
#include "stm32f103xe.h"

void Delay_Init(uint8_t SYSCLK);
void delay_ms(uint32_t ms);
void delay_us(uint32_t us);




#ifdef __cplusplus
}
#endif

#endif


