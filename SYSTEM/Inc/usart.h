#ifndef __USART__H__
#define __USART__H__

#ifdef __cplusplus
extern "C" {
#endif



#include "stm32f1xx.h"
#include "stm32f103xe.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_conf.h"

#define USART1_TX_PIN GPIO_PIN_9
#define USART1_RX_PIN GPIO_PIN_10


void USART_Init(uint32_t port);

void uart1SendChar(uint8_t ch);

void sendChar(uint8_t* ch,uint32_t size_len);



#ifdef __cplusplus
}
#endif

#endif



