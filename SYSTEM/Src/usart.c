#include "usart.h"
#include "stdio.h"


#define RXBUFFERSIZE   1 //缓存大小
#define USART1_REC_NUM  			100  	//定义最大接收字节数 200
uint8_t aRxBuffer[RXBUFFERSIZE]; //HAL库使用的串口接收缓冲

UART_HandleTypeDef UART1_Handler;

void USART_GPIO_Init(void){
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_USART1_CLK_ENABLE();


  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.Pin=GPIO_PIN_9;			//PA9
	GPIO_InitStructure.Mode=GPIO_MODE_AF_PP;		//复用推挽输出
	GPIO_InitStructure.Pull=GPIO_PULLUP;			//上拉
	GPIO_InitStructure.Speed=GPIO_SPEED_FREQ_HIGH;		//高速

	HAL_GPIO_Init(GPIOA,&GPIO_InitStructure);	   	//初始化PA9

    GPIO_InitStructure.Mode=GPIO_MODE_IT_RISING;		
	GPIO_InitStructure.Pin=GPIO_PIN_10;			//PA10
	HAL_GPIO_Init(GPIOA,&GPIO_InitStructure);	   	//初始化PA10

    	
	HAL_NVIC_EnableIRQ(USART1_IRQn);				//使能USART1中断通道
	HAL_NVIC_SetPriority(USART1_IRQn,3,3);			//抢占优先级3，子优先级3	
}


void uart1_init(uint32_t bound){
    UART1_Handler.Instance=USART1;
    UART1_Handler.Init.BaudRate=bound;
    UART1_Handler.Init.WordLength=UART_WORDLENGTH_8B;
    UART1_Handler.Init.StopBits=UART_STOPBITS_1;
    UART1_Handler.Init.Parity=UART_PARITY_NONE;
    UART1_Handler.Init.HwFlowCtl=UART_HWCONTROL_NONE;
    UART1_Handler.Init.Mode=UART_MODE_TX_RX;
   

    HAL_UART_Init(&UART1_Handler);
    HAL_UART_Receive_IT(&UART1_Handler, (uint8_t *)aRxBuffer, RXBUFFERSIZE);//该函数会开启接收中断：标志位UART_IT_RXNE，并且设置接收缓冲以及接收缓冲接收最大数据量
}

void USART_Init(uint32_t port){
    USART_GPIO_Init();
    uart1_init(port);
}




void uart1SendChar(uint8_t ch){
	 HAL_UART_Transmit(&UART1_Handler,(uint8_t *)&ch,1,0xFFFF);//阻塞方式打印,串口1
}


void sendChar(uint8_t* ch,uint32_t size_len){
    int i=0;

    for(i=0;i<size_len;i++){
        uart1SendChar(ch[i]);
    }
}


//printf重定向代码,修改其底层fputc
#if 1
#include <stdio.h>

/* 告知连接器不从C库链接使用半主机的函数 */	
#pragma import(__use_no_semihosting)

/* 定义 _sys_exit() 以避免使用半主机模式 */
void _sys_exit(int x)
{
    x = x;
}

/* 标准库需要的支持类型 */
struct __FILE
{
    int handle;
};

FILE __stdout;

/*  */
int fputc(int ch, FILE *stream)
{
	
	  uart1SendChar((uint8_t) ch);
    return ch;
}



#endif







