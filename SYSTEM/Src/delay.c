#include "delay.h"

static uint8_t fac_us=0;
static uint16_t fac_ms=0;


/**
 * @brief  初始化SysTick使用外部时钟
 * 
 * @param SYSCLK 时钟频率
 */
void Delay_Init(uint8_t SYSCLK){
	SysTick->CTRL&~(1<<2);
	fac_us=SYSCLK/8;
	fac_ms=(uint16_t)fac_us*1000;
}




void delay_ms(uint32_t ms){

    uint32_t temp;
    while (ms--)
    {
        SysTick->LOAD=(uint32_t)fac_ms;
	    SysTick->VAL=0x00;
	    SysTick->CTRL=0x01;

	    do{
		    temp=SysTick->CTRL;
	    }while((temp&0x01)&&!(temp&(1<<16)));

	    SysTick->CTRL=0x00;
	    SysTick->VAL=0x00;
    }
}




void delay_us(uint32_t us){
	uint32_t temp;
	SysTick->LOAD=us*fac_us;
	SysTick->VAL=0x00;
	SysTick->CTRL=0x01;

	do{
		temp=SysTick->CTRL;
	}while((temp&0x01)&&!(temp&(1<<16)));

	SysTick->CTRL=0x00;
	SysTick->VAL=0x00;

}


