#include "spi.h"



SPI_HandleTypeDef hspi1;  //SPI1句柄
/****************************************************************************
* 名    称: void SPI1_Init(void)
* 功    能：spi1硬件初始化
* 入口参数：无
* 返回参数：无
* 说    明：spi1初始化并且将其配置成主机模式 		     
****************************************************************************/
static void DSP_GPIO_Init(void){
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    __HAL_RCC_SPI1_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();

    GPIO_InitStruct.Pin = SPI1_SCK | SPI1_MOSI;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = SPI1_MISO;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}


void SPI1_Init(void)
{	 
	DSP_GPIO_Init();
  hspi1.Instance = SPI1; //SPI1
  hspi1.Init.Mode = SPI_MODE_MASTER;  //设置SPI工作模式，设置为主模式
  hspi1.Init.Direction = SPI_DIRECTION_2LINES; //设置SPI单向或者双向的数据模式:SPI设置为双线模式
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT; //设置SPI的数据大小:SPI发送接收8位帧结构
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW; //串行同步时钟的空闲状态为低电平
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;  //串行同步时钟的第一个跳变沿（上升或下降）数据被采样
  hspi1.Init.NSS = SPI_NSS_SOFT;  //NSS信号由硬件（NSS管脚）还是软件（使用SSI位）管理:内部NSS信号有SSI位控制
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4; ////定义波特率预分频的值:波特率预分频值为4
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB; //指定数据传输从MSB位还是LSB位开始:数据传输从MSB位开始
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;  //关闭TI模式
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE; //关闭硬件CRC校验
  hspi1.Init.CRCPolynomial = 7;  //CRC值计算的多项式
 
  HAL_SPI_Init(&hspi1);
} 


/****************************************************************************
* 名    称: void SPI1_Setclock(u8 SPI_Prescaler)
* 功    能：SPI1时钟速度设置函数
* 入口参数：SPI_Prescaler：分频系数
* 返回参数：无
* 说    明：SPI_BaudRate_Prescaler取值范围:SPI_BaudRatePrescaler_2~SPI_BaudRatePrescaler_256 	     
****************************************************************************/
void SPI1_Setclock(uint8_t SPI_Prescaler)
{
    assert_param(IS_SPI_BAUDRATE_PRESCALER(SPI_BaudRatePrescaler));//判断有效性
    __HAL_SPI_DISABLE(&hspi1);            //关闭SPI
    hspi1.Instance->CR1&=0XFFC7;          //位3-5清零，用来设置波特率
    hspi1.Instance->CR1|=SPI_Prescaler;//设置SPI速度
    __HAL_SPI_ENABLE(&hspi1);             //使能SPI
} 

//SPI1总线读写一个字节
uint8_t SPI1_ReadWriteByte(uint8_t writeData){
    uint8_t Readdata;
    HAL_SPI_TransmitReceive(&hspi1,&writeData,&Readdata,1, 1000);       
   	return Readdata; 
}  

  
		  








